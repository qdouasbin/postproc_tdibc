#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as pl
import matplotlib.ticker as tick
import matplotlib
from matplotlib import rcParams

rcParams['text.usetex'] = True
rcParams['text.latex.unicode'] = True
params1 = {
    'axes.labelsize': 20,
    'text.fontsize': 20,
    'xtick.labelsize': 23,
    'ytick.labelsize': 20,
    'lines.markersize': 10,
    'font.size': 20,
    'legend.fontsize': 16
}
rcParams.update(params1)
matplotlib.rcParams['mathtext.fontset'] = 'stix'
matplotlib.rcParams['font.family'] = 'STIXGeneral'


# --------------------------------------
# Functions
def load_AVBP_text_info(path):
    """
    Reads data in .txt format outputed by AVBP tdimp.f90
    """
    print 'Loading %s' % path
    data = np.loadtxt(path, skiprows=0)
    return data


def get_yMinMax(y, marginFactor):
    out = np.abs(np.max(np.amin(y), np.amax(y)))
    return out * marginFactor


# --------------------------------------

# --------------------------------------
# LOADIND DATA
print
'reading data'
conv = load_AVBP_text_info('../../conv_int_value_iter.txt')
dW_in = load_AVBP_text_info('../../dW_in.txt')
dW_out = load_AVBP_text_info('../../dW_out.txt')
dt = load_AVBP_text_info('../../dt.txt')
# --------------------------------------

# --------------------------------------
# Useful variables
ite = 0.5 * np.array(range(len(conv)))
rho_0 = 1.13798
a_0 = 352.8957
# --------------------------------------


# --------------------------------------
#  ACOUTSIC ENERGY 
print
'Acoustic variables computation'
nx = 1.  # Orientation of normals in AVBP

uprim = 0.5 * nx * (np.cumsum(dW_in) - np.cumsum(dW_out))
pprim = 0.5 * rho_0 * a_0 * (np.cumsum(dW_in) + np.cumsum(dW_out))
AcousticEnergy = uprim * pprim

dt_sum_ms = 1e3 * np.cumsum(dt)
dt_ms = 1e3 * dt

fig0 = pl.figure()
ax0 = fig0.add_subplot(111)
ax0.set_title("$\Delta t$")
ax0.plot(dt, linewidth=2.0, color='black')
ax0.set_ylabel('Time [ms]')
ax0.set_xlabel('Iterations [-]')
ax0.set_xlim(np.amin(0.), 2. * np.amax(ite))
pl.grid(which='major')
pl.tight_layout()

print 'Plotting uprim'
fig1 = pl.figure()
ax1 = fig1.add_subplot(111)
ax1.plot(dt_sum_ms, uprim, linewidth=2.0, color='black')
ax1.set_xlabel('Time [ms]')
ax1.set_ylabel(r"$u^{'}$ [$m.s^{-1}$]")
ax1.set_xlim(np.amin(dt_sum_ms), np.amax(dt_sum_ms))
pl.grid(which='major')
pl.tight_layout()

print 'Plotting pprim'
fig2 = pl.figure()
ax2 = fig2.add_subplot(111)
ax2.plot(dt_sum_ms, pprim, linewidth=2.0, color='black')
ax2.set_xlabel('Time [ms]')
ax2.set_ylabel(r"$p^{'}$ [Pa]")
ax2.set_xlim(np.amin(dt_sum_ms), np.amax(dt_sum_ms))
pl.grid(which='major')
pl.tight_layout()

print
'Plotting Acoustic Power'
fig3 = pl.figure()
ax3 = fig3.add_subplot(111)
ax3.plot(dt_sum_ms, AcousticEnergy, linewidth=2.0, color='black')
ax3.set_title(r'Acoustic Power Flux')
ax3.set_xlabel(r'Number of iterations [-]')
ax3.set_xlabel('Time [ms]')
ax3.set_xlim(np.amin(dt_sum_ms), np.amax(dt_sum_ms))
pl.grid(which='major')
pl.tight_layout()

print
'Plotting Cumulative Acoustic Power'
fig4 = pl.figure()
ax4 = fig4.add_subplot(111)
ax4.plot(dt_sum_ms, np.cumsum(AcousticEnergy), linewidth=2.0, color='black')
ax4.set_title(r'Total Acoustic Power Flux')
ax4.set_ylabel(r"$Total P_{ac}$ [$W.m^{-2}$]")
ax4.set_xlabel('Time [ms]')
ax4.set_xlim(np.amin(dt_sum_ms), np.amax(dt_sum_ms))
pl.grid(which='major')
pl.tight_layout()
# --------------------------------------


# --------------------------------------
# Convolution Integral 
print
'shape conv', conv.shape

print
'Plotting convolution integral'
fig5 = pl.figure()
ax5 = fig5.add_subplot(111)
ax5.plot(dt_sum_ms, conv, linewidth=2.0, color='black', label=r'Convolution Integral')
ax5.set_title(r'Value of Convolution integral')
ax5.set_ylabel(r'Conv [-]')
ylimits = get_yMinMax(conv, 1.2)
ax5.set_ylim(-ylimits, ylimits)
ax5.set_xlabel('Time [ms]')
ax5.set_xlim(np.amin(dt_sum_ms), np.amax(dt_sum_ms))
pl.grid(which='major')
pl.tight_layout()

# Setting y axis format to exponential

# xaxis
x_fmt = tick.FormatStrFormatter('%0.0f')
ax1.xaxis.set_major_formatter(tick.FuncFormatter(x_fmt))
ax2.xaxis.set_major_formatter(tick.FuncFormatter(x_fmt))
ax3.xaxis.set_major_formatter(tick.FuncFormatter(x_fmt))
ax4.xaxis.set_major_formatter(tick.FuncFormatter(x_fmt))
ax5.xaxis.set_major_formatter(tick.FuncFormatter(x_fmt))

# yaxis
y_fmt = tick.FormatStrFormatter('%0.0e')
ax1.yaxis.set_major_formatter(tick.FuncFormatter(y_fmt))
ax2.yaxis.set_major_formatter(tick.FuncFormatter(y_fmt))
ax3.yaxis.set_major_formatter(tick.FuncFormatter(y_fmt))
ax4.yaxis.set_major_formatter(tick.FuncFormatter(y_fmt))
ax5.yaxis.set_major_formatter(tick.FuncFormatter(y_fmt))
pl.tight_layout()
# pl.grid(which='major')

print
"\nSaving plot\n"
fig1.savefig('./AcousticVelocity' + '.pdf', transparent=True, dpi=400)
fig2.savefig('./AcousticPressure' + '.pdf', transparent=True, dpi=400)
fig3.savefig('./AcousticPower' + '.pdf', transparent=True, dpi=400)
fig4.savefig('./TotalAcousticPower' + '.pdf', transparent=True, dpi=400)
fig5.savefig('./ConvolutionIntegral' + '.pdf', transparent=True, dpi=400)
# --------------------------------------

print
"\nShowing plot\n"
pl.show()

print
"\nEnd of the script\n"
